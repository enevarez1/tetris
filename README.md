# tetris

#HW1
    • Write the main class Tetris and
    its play() method in a stepwise
    refinement fashion.
    • Identify the interfaces of other
    modules (Tetrominos and
    Board)
        • “Public” methods (services) that
        Tetris uses
        • Write driver methods to test
        Board and Tetrominos. (Bottom
        Up approach)
    • Identify relevant information you need to maintain in Tetris. Score? Level?
    Tetromino final position ? etc.
    • Model is not the GUI
    • The model shall detect and prevent the user from placing a tetromino on
    an invalid or inconsistent/conflicting positions in the grid.
    • Driver methods, to test methods implemented by calling them.
    • Partial implementation, not a functional Tetris at this point.

