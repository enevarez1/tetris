package model;

public class TetrominoMaker
{
    //Need to Make Tetrominos
    /*

    MAKE ALL BLOCK HORIZONTAL TO START

    I, L, J, T, O S and Z
    I is long 4 block
    L is 3 block vertical, one block on right side
    J is 3 block vertical, one block on left side
    T is 3 block horizontal, one block in the middle down
    O is 2x2 block
    S is 2 block horizontal, on 2nd block it goes up and 2 block horizontal to right
    Z is 2 block horizontal, on 1st block it goes up and 2 block horizontal to left
     */


    //Create class for each of these tetrominos
    //R2 in HW1

    private class ITetromino extends Tetromino
    {
        //I is long 4 block horizontal. Using excel to draw
        int[][] coordinates = {{1,1,1,1}};

    }
    private class LTetromino extends Tetromino
    {
        //L is 3 block horizontal, one block on right side
        int[][] coordinates = {{0,0,1},{1,1,1}};

    }
    private class JTetromino extends Tetromino
    {
        int[][] coordinates = {{1,0,0},{1,1,1}};

    }
    private class TTetromino extends Tetromino
    {
        int[][] coordinates = {{1,1,1},{0,1,0}};

    }
    private class OTetromino extends Tetromino
    {
        int[][] coordinates = {{1,1},{1,1}};

    }
    private class STetromino extends Tetromino
    {
        int[][] coordinates = {{0,1,1},{1,1,0}};

    }
    private class ZTetrimino extends Tetromino
    {
        int[][] coordinates = {{1,1,0},{0,1,1}};

    }

}