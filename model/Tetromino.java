/**
 *
 */
package model;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;

/** This class is the basis for a tetromino.
 * @author epadilla2
 * @param <U>
 * @param <T>
 *
 */
public abstract class Tetromino{

    public int[][] coordinates; //Field for coordinates of Tetrominos

    public int getHeight()
    {
        return this.coordinates.length;
    }
    public int getWidth()
    {
        return this.coordinates[0].length;
    }

    /**
     * Rotates tetromino to the right.
     */

    //R3 in HW1
    public void rotateRight()
    {
        int[][] rotatedCoords = new int[this.coordinates[0].length][this.coordinates.length];
        //if tetrimono is 1x4 it is now gonna occupy 4x1.

        for (int row = 0; row < this.coordinates.length;row++) //Using excel. Plot points and see what the transition is when you draw it out.
        {
            for (int col = 0; col < this.coordinates[0].length; col++) {
                rotatedCoords[col][this.coordinates.length - 1 - row] = this.coordinates[row][col];//Rotating rows if you are rotating right.
            }
        }
        this.coordinates = rotatedCoords; //Cords now take over the Temp array.
    }
    /**
     * Rotates tetromino to the left.
     */
    public void rotateLeft()
    {
        int[][] rotatedCoords = new int[this.coordinates[0].length][this.coordinates.length];
        //if tetrimono is 1x4 it is now gonna occupy 4x1.

        for (int row = 0; row < this.coordinates.length;row++) //Using excel. Plot points and see what the transition is when you draw it out.
        {
            for (int col = 0; col < this.coordinates[0].length; col++) {
                rotatedCoords[this.coordinates[0].length - 1 - col][row] = this.coordinates[row][col];//Rotating columns if you are rotating left.
            }
        }
        this.coordinates = rotatedCoords; //Cords now take over the Temp array.
    }

    /**
     * Enumeration to list the different tetrominos.
     * FILLER used when in multiplayer sending a line to the other player.
     * @author epadilla2
     *
     */
    public enum TetrominoEnum
    {
        /** Types of tetrominos, filler represents punishment lines added in multiplayer mode */

        I(0), J(1), L(2), O(3), S(4), Z(5), T(6), FILLER(7);
        //Constants for a switch case
        /** Integer value of each tetromino*/
        private int value;
        /**  Hash for inverse lookup of a tetromino based on value*/
        private static final Map<Integer, TetrominoEnum> reverseLookup = new HashMap<Integer, TetrominoEnum>();

        static {
            for (TetrominoEnum tetromino : TetrominoEnum.values()) {
                reverseLookup.put(tetromino.getValue(), tetromino);
            }
        }
        /**
         * Constructor that sets the integer value of tetromino
         * @param value
         */
        TetrominoEnum(int value)
        {
            this.value = value;
        }
        /**
         * Return integer value of tetromino
         * @return
         */
        public int getValue()
        {
            return value;
        }
        /**
         * Return TetrominoEnum depending on value
         * @param value
         * @return
         */
        public static TetrominoEnum getEnumByValue(int value)
        {
            return reverseLookup.get(value);
        }
        /**
         * Returns a random TetrominoEnum
         * @return
         */
        public static TetrominoEnum getRandomTetromino() {
            Random random = new Random();
            return values()[random.nextInt(values().length-1)];
        }
    }
}