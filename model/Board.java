package model;

/**
* Model class from MVC. This class contains the Tretris logic but no GUI information.
* Model must not know anything about the GUI and Controller.
* @author epadilla2
*
*/
public class Board extends java.util.Observable {

private int level;
private int score;
private boolean isGameActive;
public int[][] boardCoords = new int[20][10];
private Tetromino tetromino;
private TetrominoMaker tm;
private int xPos;
private int yPos;
public boolean [][] boardPos = new boolean[20][10];

	public Board()
	{
	}
    /**
     * Returns if game is active
     * @return
     */
    public boolean isGameActive ()
    {

        return isGameActive;
    }
    /**
     * Returns score
     * @return
     */
    public int getScore ()
    {
        return score;
    }
    /**
     * Return current level
     * @return
     */
    public int getLevel ()
    {
        return level;
    }
    /**
     * Moves tetromino down
     */
    public void moveTetrominoDown ()
    {
        //your code goes here
        this.yPos = this.yPos-1;
        validateTetrominoPosition();
    }
    public void moveTetrominoRight ()
    {
        //your code goes here
        this.xPos = this.xPos + 1;
        if (!validateTetrominoPosition()) {
            this.xPos = this.xPos - 1;
        }
    }
    public void moveTetrominoLeft ()
    {
        //your code goes here
        this.xPos = this.xPos - 1;
        if (!validateTetrominoPosition()) {
            this.xPos = this.xPos + 1;
        }
    }
    /**
     * Returns if this is a valid position
     * @return
     */
    private boolean validateTetrominoPosition ()
    {
        //your code goes here
        //First check if it can actually fit on the board, not pieces right now

        if (this.xPos < 0 || this.xPos + this.tetromino.getWidth() - 1 > 9)
            return false;
        if (this.yPos < 0 || this.yPos + this.tetromino.getHeight() - 1 > 20)
            return false;
		if (boardPos[this.xPos][this.yPos]!= true)
			return false;
        return true;
    }

    public boolean placeTetromino(){
    	if (validateTetrominoPosition() == true)
			boardPos[this.xPos][this.yPos] = true;
	}

    public void printBoard()
    {
        for(int i = 0; i < this.boardCoords.length; i++) {
            for (int j = 0; j < this.boardCoords[i].length; j++) {
                System.out.print(boardCoords[i][j]);
            }
            System.out.println();
        }
    }

    public void printTetrominoCurr()
    {

        for(int i = 0; i < this.boardCoords.length; i++) {
            for (int j = 0; j < this.boardCoords[i].length; j++) {
                System.out.print(boardCoords[i][j]);
            }
            System.out.println();
        }
    }

    public void testBoard(){
        printBoard();
        tetromino.getRandomTetromino();
        if(validateTetrominoPosition()){
            System.out.println("rotating left tetromino:");
            moveTetrominoLeft();
            System.out.println("rotating left tetromino:");
            moveTetrominoLeft();
            System.out.println("rotating right tetromino:");
            moveTetrominoRight();
            while(validateTetrominoPosition()){
                moveTetrominoDown();
            }
            placeTetromino();
        }

    }
}